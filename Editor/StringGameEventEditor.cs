using UnityEditor;
using UnityEngine;

namespace Garra.SSGE.Editor
{
    [CustomEditor(typeof(StringGameEvent), editorForChildClasses: true)]
    public class StringGameEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            StringGameEvent e = target as StringGameEvent;
            if (GUILayout.Button("Raise"))
                e.Raise(e.Arg0);
        }
    }
}

