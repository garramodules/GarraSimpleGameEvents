using UnityEditor;
using UnityEngine;

namespace Garra.SSGE.Editor
{
    [CustomEditor(typeof(FloatGameEvent), editorForChildClasses: true)]
    public class FloatGameEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            FloatGameEvent e = target as FloatGameEvent;
            if (GUILayout.Button("Raise"))
                e.Raise(e.Arg0);
        }
    }
}

