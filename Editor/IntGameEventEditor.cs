using UnityEditor;
using UnityEngine;

namespace Garra.SSGE.Editor
{
    [CustomEditor(typeof(IntGameEvent), editorForChildClasses: true)]
    public class IntGameEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            IntGameEvent e = target as IntGameEvent;
            if (GUILayout.Button("Raise"))
                e.Raise(e.Arg0);
        }
    }
}

