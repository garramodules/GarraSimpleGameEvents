using UnityEditor;
using UnityEngine;

namespace Garra.SSGE.Editor
{
    [CustomEditor(typeof(ObjectGameEvent), editorForChildClasses: true)]
    public class ObjectGameEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            ObjectGameEvent e = target as ObjectGameEvent;
            if (GUILayout.Button("Raise"))
                e.Raise(e.Arg0);
        }
    }
}

