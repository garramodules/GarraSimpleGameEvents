﻿namespace Garra.SSGE
{
    public interface IGameEvent<in TAction>
    {
        void AddEventListener(TAction listener);
        void RemoveEventListener(TAction listener);
    }
}