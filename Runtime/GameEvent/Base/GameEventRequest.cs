using System;

namespace Garra.SSGE
{
    public abstract class GameEventRequest<T0, TRes>: GameEventRequestBase<Action<T0>, Action<TRes>>
    {
        public void Raise(T0 arg, Action<TRes> response)
        {
            AddRequest(response);
            for (int i = 0; i < _listeners.Count; i++)
            {
                _listeners[i].Invoke(arg);
            }
        }
        
        public void Raise(T0 arg)
        {
            for (int i = 0; i < _listeners.Count; i++)
            {
                _listeners[i].Invoke(arg);
            }
        }
        
        public void Response(TRes res)
        {
            for (int i = 0; i < _responses.Count; i++)
            {
                _responses[i].Invoke(res);
                RemoveRequest(_responses[i]);
            }
        }
    }
}