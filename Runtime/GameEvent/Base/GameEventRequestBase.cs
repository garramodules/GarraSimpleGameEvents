using System.Collections.Generic;
using UnityEngine;

namespace Garra.SSGE
{
    public abstract class GameEventRequestBase<TAction, TResponse> : ScriptableObject,
        IGameEventRequest<TResponse>, IGameEvent<TAction>
    {
        protected List<TAction> _listeners = new List<TAction>();
        protected List<TResponse> _responses = new List<TResponse>();
        
        public void AddEventListener(TAction listener)
        {
            if (!_listeners.Contains(listener))
            {
                _listeners.Add(listener);
            }
        }

        public void RemoveEventListener(TAction listener)
        {
            if (_listeners.Contains(listener))
            {
                _listeners.Remove(listener);
            }
        }

        public void AddRequest(TResponse response)
        {
            if (!_responses.Contains(response))
            {
                _responses.Add(response);
            }
        }

        public void RemoveRequest(TResponse response)
        {
            if (_responses.Contains(response))
            {
                _responses.Remove(response);
            }
        }
    }
}