using System.Linq;

namespace Garra.SSGE
{
    public interface IGameEventRequest<in TResponse>
    {
        void AddRequest(TResponse response);
        void RemoveRequest(TResponse response);
    }
}