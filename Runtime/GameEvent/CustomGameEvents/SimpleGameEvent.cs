﻿using UnityEngine;

namespace Garra.SSGE
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "MyGameEvents/GameEvent")]
    public class SimpleGameEvent : GameEvent
    {

    }
}